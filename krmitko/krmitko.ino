#include "LiquidCrystal_I2C.h"  // Knihovna pro obsluhu LCD displeje s I2C modulem, Správa knihoven -> LiquidCrystal I2C by Marco Schwartz
#include <IRremote.hpp>

LiquidCrystal_I2C lcd(0x27, 16, 2); //Inicializace LCD dipleje s výchozí adresou I2C 0x27, 16 sloupců, 2 řádky

const int servoPin = 9;

int feedingLength = 5;
long intervalSeconds = 12l*3600l; 
long lastFeedingTime;
boolean optionMode = false;
boolean fullDayMode = false;

void setup() {
  lcd.init();                   
  lcd.backlight();              
  lcd.setCursor(0,0);   
  
  Serial.begin(9600);
  pinMode(servoPin, OUTPUT);
  lastFeedingTime = getElapsedTimeInSeconds(); 
  IrReceiver.begin(2, false);
}

void loop() {  
  // Nastavení ovládání 
  boolean forceFeeding = setupApplicationControlls();

  // Krmení
  float currentTime = getElapsedTimeInSeconds();
  float nextFeedTime = lastFeedingTime + intervalSeconds;
  if(forceFeeding || currentTime >= nextFeedTime) {
    // Vypsání hlášky při krmení
    printFeedingInfo();
    
    // Nakrmení ryb
    feedFish();

    // Nastavení času posledního krmení
    lastFeedingTime = currentTime;
  }

  // Výpis na display
  if(optionMode){
    // V módu pro nastavení - vypsání detailů nastavení
    printOptionDetails();
  } else {
    // Vypsání času do dalšího krmení
    printTimeLcd(currentTime, nextFeedTime);
  }
  
  // Delay na konci
  delay(1000);
}

// Metoda vracející uplynulý čas v sekundách
float getElapsedTimeInSeconds(){
  return millis() / 1000; 
}

// Metoda nastavující ovládání aplikace
boolean setupApplicationControlls() {
  boolean forceFeeding = false;
  if (IrReceiver.decode()) {
    Serial.println(IrReceiver.decodedIRData.decodedRawData, HEX);
    IrReceiver.printIRResultShort(&Serial); // optional use new print version
    switch(IrReceiver.decodedIRData.decodedRawData){
      // Tlačítko EQ - pro vstup do nastavení
      case 0xF609FF00:
        optionMode = !optionMode;
        break;
      // Tlačítko CH - pro nastavení intervalu krmení
      case 0xB946FF00: 
        fullDayMode = !fullDayMode;
        if(fullDayMode){
          intervalSeconds = 24l*3600l;
        } else {
          intervalSeconds = 12l*3600l;
        }
        break;
      // Tlačítko "-" - pro zkrácení doby krmení
      case 0xF807FF00:
        if(feedingLength > 1) {
          feedingLength--;
        } 
        break;
      // Tlačítko "+" - pro prodloužení doby krmení
      case 0xEA15FF00: 
        feedingLength++;
        break;
      // Tlačítko play/pause - pro okamžité spuštění krmení
      case 0xBC43FF00: 
        forceFeeding = true; 
        
    }
    IrReceiver.resume();
  }  
  return forceFeeding;
}

// Metoda pro vypsání hlášky při krmení
void printFeedingInfo() {
  lcd.clear();
  lcd.print("Feeding... ");
  lcd.home();
}

// Metoda pro krmení ryb
void feedFish(){
  analogWrite(servoPin, HIGH);
  delay(feedingLength*1000);
  analogWrite(servoPin, LOW);
  delay(10);
}

// Metoda pro výpis detailů nastavení
void printOptionDetails(){
  lcd.clear();
  lcd.print("Feeding in: ");
  lcd.print(fullDayMode ? "24h" : "12h");
  lcd.setCursor(0,1);
  lcd.print("Duration: ");
  lcd.print(feedingLength);
  lcd.print("s");
  lcd.home();  
}

// Metoda pro vypsání času do dalšího krmení
void printTimeLcd(float currentTime, float nextFeedTime){
  lcd.clear();
  lcd.print("Time to feed: ");
  lcd.setCursor(0,1);

  float timeLeft = nextFeedTime - currentTime;
  long m = timeLeft / 60;
  long s = (long)timeLeft % 60;
  long h = m / 60;
  m = m % 60;
 
  lcd.print(h);
  lcd.print(":");
  lcd.print((long)m);
  lcd.print(":");
  lcd.print(s);
  lcd.home();  
}
